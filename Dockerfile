FROM openjdk:11-jre-slim
EXPOSE 8082
ADD target/sendmail-0.1.jar sendmail-0.1.jar
ENTRYPOINT ["java", "-jar","sendmail-0.1.jar"]
